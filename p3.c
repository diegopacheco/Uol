









































































int main(){

elemento raiz=NULL, aux;
int opcao=0;
int cod, achou, disc_remov;
char disc[50]; 

while (opcao != 7){
system("cls");
printf("\n\n----------------MENU PRINCIPAL----------------");
printf("\n\n 1 - Inserir nova disciplina");
printf("\n 2 - Listar em ordem");
printf("\n 3 - Listar em preordem");
printf("\n 4 - Listar em posordem");
printf("\n 5 - Remover elemento");
printf("\n 7 - Sair");
printf("\n\n Opcao Escolhida: ");
scanf("%d",&opcao);
switch(opcao){
case 1: system("cls");
printf("\nNovo codigo: ");
scanf("%d",&cod);
printf("\nNova disciplina: ");
scanf("%s",&disc);
inserir(&raiz, cod, disc);
system("pause>>null");
break;
case 2: system("cls");
if (raiz == NULL)
puts("Arvore vazia");
else
ordem(raiz);
getch();
break;
case 3: system("cls");
if (raiz == NULL)
puts("Arvore vazia");
else
preordem(raiz);
getch();
break;
case 4: system("cls");
if (raiz == NULL)
puts("Arvore vazia");
else
posordem(raiz);
getch();
break;
case 5: system("cls");
if (raiz == (struct arvore *) NULL){
puts("\n\nA arvore vazia!");
}
else
{
printf("\nDigite o codigo da disciplina a ser removida: ");
scanf("%d", &disc_remov);
return 0;
}






































#include <stdio.h>
#include <stdlib.h>

struct Arv
{
	int num;
	struct Arv *esq;
	struct Arv *dir;
};
typedef struct Arv Arv;

void criarArv(Arv **pRaiz){
	*pRaiz = NULL;
}

void inserir(Arv **pRaiz, int num){
	if (*pRaiz == NULL)
	{
		*pRaiz = (Arv *)malloc(sizeof(Arv));
		(*pRaiz)->esq = NULL;
		(*pRaiz)->dir = NULL;
		(*pRaiz)->num = num;
	}
	else{
		if (num<(*pRaiz)->num)
		{
			inserir(&(*pRaiz)->esq,	num);
		}
		if (num > (*pRaiz)->num)
		{
			inserir(&(*pRaiz)->dir, num);
		}
	}
}

Arv *MaiorDir(Arv **arv){
	if ((*arv)->dir != NULL)
	{
		return MaiorDir(&(*arv)->dir);
	}
	else{
		Arv *aux = *arv;
		if ((*arv)->esq != NULL)	
		{
			*arv = (*arv)->esq;
		}
		else{
			*arv = NULL;
		}
		return aux;
	}
}

Arv *MenorEsq(Arv **arv){
	if ((*arv)->esq != NULL)
	{
		return MenorEsq(&(*arv)->esq);
	}
	else{
		Arv *aux = *arv;
		if ((*arv)->dir != NULL)
		{
			*arv = (*arv)->dir;
		}
		else{
			*arv = NULL;
		}
		return aux;
	}
}

void remover(Arv **pRaiz, int num){
	if (*pRaiz == NULL)
	{
		printf("Nº nao existente\n");
		return;
	}
	if (num<(*pRaiz)->num)
	{
		remover(&(*pRaiz)->esq, num);
	}
	else{
		if (num > (*pRaiz)->num)
		{
			remover(&(*pRaiz)->dir, num);
		}
		else{
			Arv *pAux = *pRaiz;
			if (((*pRaiz)->esq == NULL) && ((*pRaiz)->dir == NULL))
			{
				free(pAux);
				(*pRaiz) == NULL;
			}
			else{
				if ((*pRaiz)->esq == NULL)
				{
					(*pRaiz) = (*pRaiz)->dir;
					pAux->dir = NULL;
					free(pAux);
					pAux = NULL;
				}
				else{
					if ((*pRaiz)->dir == NULL)
					{
						(*pRaiz) = (*pRaiz)->esq;
						pAux->esq = NULL;
						free(pAux);
						pAux = NULL;
					}
					else{
						pAux = MaiorDir(&(*pRaiz)->dir);
						pAux->esq = (*pRaiz)->esq;
						pAux->dir = (*pRaiz)->dir;
						(*pRaiz)->esq = (*pRaiz)->dir = NULL;
						free((*pRaiz));
						*pRaiz = pAux;
						pAux = NULL;
					}
				}
			}
		}
	}
}

void ImprimirEmOrdem(Arv *pRaiz){
	if (pRaiz != NULL)
	{
		ImprimirEmOrdem(pRaiz->esq);
		printf("\n%i", pRaiz->num);
		ImprimirEmOrdem(pRaiz->dir);	
	}
}

void ImprimirEmPreOrdem(Arv *pRaiz){
	if (pRaiz != NULL)
	{
		printf("\n%i",pRaiz->num);
		ImprimirEmPreOrdem(pRaiz->esq);
		ImprimirEmPreOrdem(pRaiz->dir);
	}
}

void ImprimirEmPosOrdem(Arv *pRaiz){
	if (pRaiz!= NULL)
	{
		ImprimirEmPosOrdem(pRaiz->esq);
		ImprimirEmPosOrdem(pRaiz->dir);
		printf("\n%i",pRaiz->num);
	}
}

int contarNos(No *pRaiz){
   if(pRaiz == NULL)
        return 0;
   else
        return 1 + contarNos(pRaiz->esquerda) + contarNos(pRaiz->direita);
}

int contarFolhas(No *pRaiz){
   if(pRaiz == NULL)
        return 0;
   if(pRaiz->esquerda == NULL && pRaiz->direita == NULL)
        return 1;
   return contarFolhas(pRaiz->esquerda) + contarFolhas(pRaiz->direita);
}

int maior(int a, int b){
    if(a > b)
        return a;
    else
        return b;
}



int altura(No *pRaiz){
   if((pRaiz == NULL) || (pRaiz->esquerda == NULL && pRaiz->direita == NULL))
       return 0;
   else
       return 1 + maior(altura(pRaiz->esquerda), altura(pRaiz->direita));
}

int main()
{
	
	return 0;
}